# yagl4j

## Description
Yet Another Gamepad Library 4 Java.

## Motivation
There is no reactive game-pad library for Java.

## Installation
The following packages are required:
- [GCC](https://gcc.gnu.org/install/binaries.html)
- [SDL (Simple Directmedia Layer) 2](https://www.libsdl.org/download-2.0.php)
- [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Contributors
- Mark Lefering

## License
TODO
